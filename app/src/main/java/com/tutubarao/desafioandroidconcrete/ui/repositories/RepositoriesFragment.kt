package com.tutubarao.desafioandroidconcrete.ui.repositories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.tutubarao.desafioandroidconcrete.Injection
import com.tutubarao.desafioandroidconcrete.R
import kotlinx.android.synthetic.main.repositories_fragment.*

class RepositoriesFragment : Fragment() {

    private val adapter = RepositoriesAdapter()

    private lateinit var viewModel: RepositoriesViewModel

    companion object {
        fun newInstance() = RepositoriesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.repositories_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val decoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        repositoriesList.addItemDecoration(decoration)

        repositoriesList.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        context?.let {
            viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(it)).get(RepositoriesViewModel::class.java)
            viewModel.repositories.observe(this, Observer(adapter::submitList))
        }

    }
}
