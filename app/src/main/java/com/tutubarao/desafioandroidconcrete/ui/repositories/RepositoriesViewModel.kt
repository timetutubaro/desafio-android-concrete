package com.tutubarao.desafioandroidconcrete.ui.repositories

import androidx.lifecycle.ViewModel
import com.tutubarao.desafioandroidconcrete.data.source.GithubRepository

class RepositoriesViewModel(repository: GithubRepository) : ViewModel() {

    val repositories = repository.fetchRepositories()
}
