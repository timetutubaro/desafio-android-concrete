package com.tutubarao.desafioandroidconcrete.ui.repositories

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tutubarao.desafioandroidconcrete.R
import com.tutubarao.desafioandroidconcrete.data.Repository
import kotlinx.android.synthetic.main.item_repositories_list.view.*

class RepositoriesAdapter: PagedListAdapter<Repository, RepositoriesItemViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoriesItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_repositories_list, parent, false)
        return RepositoriesItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RepositoriesItemViewHolder, position: Int) {
        val repository = getItem(position)
        if (repository != null) holder.bindTo(repository)

    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Repository>() {
            override fun areItemsTheSame(oldItem: Repository, newItem: Repository): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Repository, newItem: Repository): Boolean {
                return oldItem.id == newItem.id && oldItem.fullName == newItem.fullName
            }
        }
    }

}

class RepositoriesItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bindTo(repository: Repository) {
        Picasso.get().load(repository.owner.avatarUrl).into(itemView.ownerImage)
        itemView.repositoryName.text = repository.fullName
        itemView.repositoryDescription.text = repository.description
        itemView.forksCount.text = repository.forks.toString()
        itemView.starsCount.text = repository.stars.toString()
    }
}