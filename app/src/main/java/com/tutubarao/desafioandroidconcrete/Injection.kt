package com.tutubarao.desafioandroidconcrete

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.tutubarao.desafioandroidconcrete.data.source.GithubRepository
import com.tutubarao.desafioandroidconcrete.data.source.local.GithubDb
import com.tutubarao.desafioandroidconcrete.data.source.local.GithubLocalCache
import com.tutubarao.desafioandroidconcrete.data.source.remote.GithubServiceAPI
import com.tutubarao.desafioandroidconcrete.ui.ViewModelFactory
import java.util.concurrent.Executors

/**
 * Each ViewModel that communicates with Github data should receive GithubRepository
 * in its constructor via injection. Define the calls for the provider factories in here
 */
object Injection {

    private fun provideCache(context: Context): GithubLocalCache {
        val database = GithubDb.getInstance(context)
        return GithubLocalCache(database.repositoriesDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideGithubRepository(context: Context): GithubRepository {
        return GithubRepository(GithubServiceAPI.create(), provideCache(context))
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideGithubRepository(context))
    }
}