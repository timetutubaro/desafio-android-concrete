package com.tutubarao.desafioandroidconcrete.data.source

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.tutubarao.desafioandroidconcrete.data.Repository
import com.tutubarao.desafioandroidconcrete.data.source.local.GithubLocalCache
import com.tutubarao.desafioandroidconcrete.data.source.remote.GithubServiceAPI

class GithubRepository(
        private val serviceAPI: GithubServiceAPI,
        private val cache: GithubLocalCache
) {
    fun fetchRepositories(): LiveData<PagedList<Repository>> {
        val dataSourceFactory = cache.fetchRepositories()

        val boundaryCallback = RepositoryBoundaryCallback(serviceAPI, cache)

        // We should expose network errors in a different class in the future
        val networkErrors = boundaryCallback.networkErrors

        return LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
                .setBoundaryCallback(boundaryCallback)
                .build()
    }

    companion object {
        const val DATABASE_PAGE_SIZE = 30
    }
}