package com.tutubarao.desafioandroidconcrete.data

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class PullRequest(
        @PrimaryKey @field:SerializedName("id") val id: Long,
        @field:SerializedName("title") val title: String,
        @field:SerializedName("body") val body: String
)