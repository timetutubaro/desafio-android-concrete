package com.tutubarao.desafioandroidconcrete.data.source.local

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tutubarao.desafioandroidconcrete.data.Repository

@Dao
interface RepositoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(repository: List<Repository>)

    @Query("SELECT * FROM repository ORDER BY stars DESC")
    fun fetch(): DataSource.Factory<Int, Repository>
}