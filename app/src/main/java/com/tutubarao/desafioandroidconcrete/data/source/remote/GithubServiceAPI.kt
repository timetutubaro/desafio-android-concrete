package com.tutubarao.desafioandroidconcrete.data.source.remote

import com.tutubarao.desafioandroidconcrete.data.PullRequest
import com.tutubarao.desafioandroidconcrete.data.Repository
import com.tutubarao.desafioandroidconcrete.data.RepositorySearchResponse
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface GithubServiceAPI {

    @GET("/search/repositories")
    fun searchRepositories(@QueryMap options: Map<String, String>): Call<RepositorySearchResponse>

    @GET("/repos/{creator}/{repository}/pulls")
    fun getPullrequestsRepository(@Path("creator") creator: String, @Path("repository") repository: String): Call<List<PullRequest>>

    companion object {
        private const val BASE_URL = "https://api.github.com/"
        fun create(): GithubServiceAPI = create(HttpUrl.parse(BASE_URL)!!)

        fun create(httpUrl: HttpUrl): GithubServiceAPI = Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubServiceAPI::class.java)
    }
}