package com.tutubarao.desafioandroidconcrete.data

import com.google.gson.annotations.SerializedName

data class RepositorySearchResponse(
        @field:SerializedName("total_count") val totalCount: Long,
        @field:SerializedName("incomplete_results") val incompleteResults: Boolean,
        @field:SerializedName("items") val items: List<Repository>
)