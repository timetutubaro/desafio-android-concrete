package com.tutubarao.desafioandroidconcrete.data.source

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.tutubarao.desafioandroidconcrete.data.Repository
import com.tutubarao.desafioandroidconcrete.data.RepositorySearchResponse
import com.tutubarao.desafioandroidconcrete.data.source.local.GithubLocalCache
import com.tutubarao.desafioandroidconcrete.data.source.remote.GithubServiceAPI
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoryBoundaryCallback(
        private val serviceAPI: GithubServiceAPI,
        private val cache: GithubLocalCache) : PagedList.BoundaryCallback<Repository>() {

    private var lastRequestedPage: Int = 1

    private val _networkErrors = MutableLiveData<String>()
    val networkErrors: LiveData<String>
        get() = _networkErrors

    private var isRequestInProgress: Boolean = false

    @MainThread
    override fun onItemAtEndLoaded(itemAtEnd: Repository) {
        requestAndSaveData()
    }

    override fun onZeroItemsLoaded() {
        requestAndSaveData()
    }

    fun requestAndSaveData() {
        if (isRequestInProgress) return

        isRequestInProgress = true
        request({ repositories ->
            cache.insert(repositories) {
                lastRequestedPage++
                isRequestInProgress = false
            }
        }, { error ->
            _networkErrors.postValue(error)
            isRequestInProgress = false
        })
    }

    private fun request(
            onSuccess: (repositories: List<Repository>) -> Unit,
            onError: (error: String) -> Unit) {
        val query: HashMap<String, String> = HashMap()

        query.put("q", "java")
        query.put("sort", "stars")
        query.put("page", lastRequestedPage.toString())

        serviceAPI.searchRepositories(query).enqueue(
                object : Callback<RepositorySearchResponse> {
                    override fun onResponse(
                            call: Call<RepositorySearchResponse>,
                            response: Response<RepositorySearchResponse>) {
                        if (response.isSuccessful) {
                            onSuccess(response.body()?.items ?: emptyList())
                        } else {
                            onError(response.errorBody()?.string() ?: "Unknown error")
                        }
                    }

                    override fun onFailure(call: Call<RepositorySearchResponse>?, t: Throwable) {
                        onError(t.message ?: "Unknown error")
                    }
                }
        )
    }
}