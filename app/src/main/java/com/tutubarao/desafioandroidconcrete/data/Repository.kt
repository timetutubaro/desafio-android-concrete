package com.tutubarao.desafioandroidconcrete.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Repository(
        @PrimaryKey @field:SerializedName("id") val id: Long,
        @field:SerializedName("full_name") val fullName: String,
        @field:SerializedName("description") val description: String? = null,
        @field:SerializedName("html_url") val url: String,
        @field:SerializedName("stargazers_count") val stars: Int,
        @field:SerializedName("forks_count") val forks: Int,
        @Embedded(prefix = "owner") @field:SerializedName ("owner") val owner: Owner
)

