package com.tutubarao.desafioandroidconcrete.data

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class Owner(
        @SerializedName("login") val login: String,
        @SerializedName("avatar_url") val avatarUrl: String
)