package com.tutubarao.desafioandroidconcrete.data.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.tutubarao.desafioandroidconcrete.data.Repository

@Database(
        entities = [Repository::class],
        version = 1,
        exportSchema = false
)
abstract class GithubDb : RoomDatabase() {
    abstract fun repositoriesDao(): RepositoryDao

    companion object {

        @Volatile
        private var INSTANCE: GithubDb? = null

        fun getInstance(context: Context): GithubDb =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: create(context).also { db -> INSTANCE = db }
                }

        private fun create(context: Context): GithubDb = Room
                .databaseBuilder(context, GithubDb::class.java, "github.db")
                // It's not a problem in this context
                .fallbackToDestructiveMigration()
                .build()
    }
}