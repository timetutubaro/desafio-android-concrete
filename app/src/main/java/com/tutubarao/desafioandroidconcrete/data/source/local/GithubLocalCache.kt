package com.tutubarao.desafioandroidconcrete.data.source.local

import androidx.paging.DataSource
import com.tutubarao.desafioandroidconcrete.data.Repository
import java.util.concurrent.Executor

class GithubLocalCache(
        private val repositoryDao: RepositoryDao,
        private val ioExecutor: Executor
) {
    fun insert(repositories: List<Repository>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            repositoryDao.insert(repositories)
            insertFinished()
        }
    }

    fun fetchRepositories(): DataSource.Factory<Int, Repository> = repositoryDao.fetch()
}